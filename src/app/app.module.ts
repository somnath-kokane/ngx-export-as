import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from './app.component';

import {  ExportAsModule, ExportAsEntryComponent } from "./export-as";

const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'export-as', pathMatch: 'full' },
  { path: 'export-as', component: ExportAsEntryComponent }
]

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(APP_ROUTES),
    ExportAsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
