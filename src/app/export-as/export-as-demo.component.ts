import { Component, OnInit } from '@angular/core';

import { ExportAsService, AOA } from "./export-as.service";

@Component({
  selector: 'export-as-demo',
  templateUrl: 'export-as-demo.component.html'
})

export class ExportAsDemoComponent implements OnInit {

  data:AOA = [[1, 2], [3, 4]]
  fileName: string = 'SheetJS.xlsx';

  constructor(
    private exportAs: ExportAsService
  ) { }

  ngOnInit() { }

  onExportAsExcel(data, idx){
    this.exportAs.excel([data], `Sheet-${idx}.xlsx`)
  }

  onExportAsPdf(data, idx){
    this.exportAs.pdf([data], `pdf-${idx}.pdf`)
  }

}