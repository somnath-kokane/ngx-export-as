import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { ExportAsEntryComponent } from "./export-as.entry.component";
import { ExportAsDemoComponent } from './export-as-demo.component';
import { ExportAsService } from "./export-as.service";

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [],
  entryComponents: [ExportAsEntryComponent],
  declarations: [ExportAsDemoComponent, ExportAsEntryComponent],
  providers: [ExportAsService],
})
export class ExportAsModule { }
