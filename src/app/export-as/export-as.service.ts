import { Injectable } from '@angular/core';

// import * as jsPDF from 'jspdf';
import * as XLSX from 'xlsx';
import { saveAs } from 'file-saver';

declare var jsPDF: any; // Important

export type AOA = Array<Array<any>>;

const s2ab = (s: string): ArrayBuffer => {
  const buf: ArrayBuffer = new ArrayBuffer(s.length);
  const view: Uint8Array = new Uint8Array(buf);
  for (let i = 0; i !== s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
  return buf;
}

@Injectable()
export class ExportAsService {

  constructor() { }

  excel(data, fileName) {
    const wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'binary' };
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(data);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    const wbout: string = XLSX.write(wb, wopts);
    saveAs(new Blob([s2ab(wbout)]), fileName);
  }

  pdf(data, fileName){
    const pdf = new jsPDF('1', 'pt', 'a4')
    pdf.autoTable(['Column 1', 'Column 2'], data);
    pdf.save(fileName);
  }
}